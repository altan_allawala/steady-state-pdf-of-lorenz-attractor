clear

Nx = 160;
Lx = 14.;
Ly = 20.;
Lz = 20.;

dx = Lx/(Nx-1);
dy = Ly/(Nx-1);
dz = Lz/(Nx-1);

vx = linspace(-Lx/2.,Lx/2.,Nx);
vy = linspace(-Ly/2.,Ly/2.,Nx);
vz = linspace(25.-Lz/2.,25.+Lz/2.,Nx); % Domain

sigma = 3.;
rho = 26.5;
beta = 0.16;%8./3.;
gamma = 0.02

iden = speye(Nx);

N = linspace(1,Nx,Nx);
xcoord = sparse(N,N,vx,Nx,Nx);
ycoord = sparse(N,N,vy,Nx,Nx);
zcoord = sparse(N,N,vz,Nx,Nx);

A = zeros(1,Nx);
A(2) = 1;
D1 = toeplitz(-A,A);
D1 = sparse(D1);

B = zeros(1,Nx);
B(1) = -2;
B(2) = 1;
D2 = toeplitz(B,B);
D2 = sparse(D2);

identity = kron(kron(iden, iden), iden);

x = kron(kron(xcoord, iden), iden);
y = kron(kron(iden, ycoord), iden);
z = kron(kron(iden, iden), zcoord);

Dx = kron(kron(D1, iden), iden)/(2.*dx);
Dy = kron(kron(iden, D1), iden)/(2.*dy);
Dz = kron(kron(iden, iden), D1)/(2.*dz);

D2x = kron(kron(D2, iden), iden)/dx^2;
D2y = kron(kron(iden, D2), iden)/dy^2;
D2z = kron(kron(iden, iden), D2)/dz^2;

laplacian = D2x + D2y + D2z;

H = Dx*(sigma*(x-y)) + Dy*(y-rho*x+x*z) + Dz*(beta*z-x*y) + gamma*laplacian; % FPE operator for Lorenz attractor

disp('Number of non-zero entries is'), disp(nnz(H));

opts.jmin = 2;
opts.jmax = 4;
opts.MaxIt = 100; % Default 100
opts.LS_MaxIt = 5; % Default 5
opts.Disp = 1; % Display shiznit
opts.Tol = 1.e-5;
opts.Precond = H;

tic;
[V,E] = jdqr(H, 1, 0, opts);
toc;
saveas(gca,'iter.jpg')
size(V)
disp('Tic toc time is'), disp(toc);

E = diag(E)
[energy, index] = min(abs(E));

disp('Selected energy is'), disp(energy);

psi = V(:,index);

psi = real(psi);
psi = psi/sum(psi);
psi = reshape(psi,Nx,Nx,Nx);

psixy = sum(psi,1); % Three axial projections
psixz = sum(psi,2);
psiyz = sum(psi,3);

psixy = reshape(psixy,Nx*Nx,1);
psixz = reshape(psixz,Nx*Nx,1);
psiyz = reshape(psiyz,Nx*Nx,1);

save psixy psixy -ASCII;
save psixz psixz -ASCII;
save psiyz psiyz -ASCII;
