#!/usr/bin/env python

# Plot the PDF of the stochastically-forced Lorenz attractor. Written by Altan Allawala.

from numpy import *
import scipy.linalg as scilin
from pylab import *
from scipy import sparse
from scipy import io
from StringIO import StringIO
from scipy.sparse import lil_matrix
import scipy.sparse.linalg as sp
from scipy import weave
from scipy.weave import converters
from matplotlib.colors import LogNorm
from mpl_toolkits.axes_grid1 import make_axes_locatable


set_printoptions(threshold=nan)

class Lorenz:

	"""Lorenz attractor where H = Ldagger L"""


	def __init__(self, Lx=60., Ly=50., Lz=48.):

		self.Lx = Lx # Grid runs from -L/2 to L/2
		self.Ly = Ly
		self.Lz = Lz



	def LoadData(self): # Plot the PDF projects

		self.psixy = np.loadtxt('psixy')
		self.psixz = np.loadtxt('psixz')
		self.psiyz = np.loadtxt('psiyz')

		self.Nx = pow(len(self.psixy),1./2.)
		self.Nx = int(round(self.Nx))
		print 'Number of points per side is', self.Nx

		self.dx = self.Lx / (self.Nx-1.)
		self.dy = self.Ly / (self.Nx-1.)
		self.dz = self.Lz / (self.Nx-1.)

		self.xarray = linspace(-self.Lx/2., self.Lx/2., self.Nx)
		self.yarray = linspace(-self.Ly/2., self.Ly/2., self.Nx)
		self.zarray = linspace(23.-self.Lz/2., 23.+self.Lz/2., self.Nx)

		self.psixy = self.psixy/(sum(self.psixy)*self.dx*self.dy)
		self.psixz = self.psixz/(sum(self.psixz)*self.dx*self.dz)
		self.psiyz = self.psiyz/(sum(self.psiyz)*self.dy*self.dz)

		self.psixy = np.reshape(self.psixy, (self.Nx,self.Nx))
		self.psixz = np.reshape(self.psixz, (self.Nx,self.Nx))
		self.psiyz = np.reshape(self.psiyz, (self.Nx,self.Nx))


	def Cumulants2(self):

		self.Cumulants = zeros(7)
		self.Cumulants[0] = 1.

		for i in range(self.Nx):
			for j in range(self.Nx):
				self.Cumulants[1] += self.psixy[i,j]*self.xarray[i]*self.dx*self.dy
				self.Cumulants[4] += self.psixy[i,j]*self.xarray[i]**2*self.dx*self.dy

				self.Cumulants[2] += self.psixy[i,j]*self.yarray[j]*self.dx*self.dy
				self.Cumulants[5] += self.psixy[i,j]*self.yarray[j]**2*self.dx*self.dy

		for i in range(self.Nx):
			for k in range(self.Nx):
				self.Cumulants[3] += self.psixz[i,k]*self.zarray[k]*self.dx*self.dz
				self.Cumulants[6] += self.psixz[i,k]*self.zarray[k]**2*self.dx*self.dz


		print "L1 norm = ", self.Cumulants[0]
		print "<x> = ", self.Cumulants[1]
		print "<y> = ", self.Cumulants[2]
		print "<z> = ", self.Cumulants[3]
		print "<x*x> - <x>*<x> = ", self.Cumulants[4]-self.Cumulants[1]**2
		print "<y*y> - <y>*<y> = ", self.Cumulants[5]-self.Cumulants[2]**2
		print "<z*z> - <z>*<z> = ", self.Cumulants[6]-self.Cumulants[3]**2


	def PDFplots(self): # Plot the PDF projects
		scalingxy = 0.22
		scalingxz = 0.4
		scalingyz = 0.35

		p1 = 3*48
		p2 = 3*64

		p3 = 3*60
		p4 = 3*65

		p5 = 3*60
		p6 = 3*88

		figure()
		imshow(transpose(self.psixy), extent = (-self.Lx/2., self.Lx/2., -self.Ly/2., self.Ly/2.), origin='lower', aspect='auto', interpolation='none', cmap = cm.Greys, norm=mpl.colors.PowerNorm(scalingxy))
		#axis([-6.5, 6.5, -9., 9.])
		axvline(x=-self.Lx/2. + p1*self.dx, color='r', label='x=-5')
		axhline(y=-self.Ly/2. + p2*self.dy, color='r')
		colorbar()
		title(r'PDF projection on to xy plane')
#		legend()
		savefig('xy.pdf')

		figure()
		plot(self.yarray, self.psixy[p1,:], 'b-')
		xlabel('y')
		ylabel('PDF')
		title(r'x-slice of xy plane')
		savefig('xy Cross-section 1.pdf')

		figure()
		plot(self.xarray, self.psixy[:,p2], 'b-')
		xlabel('x')
		ylabel('PDF')
		title(r'y-slice of xy plane')
		savefig('xy Cross-section 2.pdf')

		figure()
		imshow(transpose(self.psixz), extent = (-self.Lx/2., self.Lx/2., 23.-self.Lz/2., 23.+self.Lz/2.), origin='lower', aspect='auto', interpolation='none', cmap = cm.Greys, norm=mpl.colors.PowerNorm(scalingxz))
		#axis([-6.5, 6.5, 15.5, 34.5])
		axvline(x=-self.Lx/2. + p3*self.dx, color='r')
		axhline(y=23.-self.Lz/2. + p4*self.dz, color='r')
		axis([-11., 11., 7., 42.])
		colorbar()
		title(r'PDF projection on to xz plane')
		savefig('xz.pdf')

		figure()
		plot(self.zarray, self.psixz[p3,:], 'b-')
		xlabel('z')
		ylabel('PDF')
		title(r'x-slice of xz plane')
		savefig('xz Cross-section 1.pdf')

		figure()
		plot(self.xarray, self.psixz[:,p4], 'b-')
		xlabel('x')
		ylabel('PDF')
		title(r'z-slice of xz plane')
		savefig('xz Cross-section 2.pdf')

		figure()
		imshow(transpose(self.psiyz), extent = (-self.Ly/2., self.Ly/2., 23.-self.Lz/2., 23.+self.Lz/2.), origin='lower', aspect='auto', interpolation='none', cmap = cm.Greys, norm=mpl.colors.PowerNorm(scalingyz))
		#axis([-9., 9., 15.5, 34.5])
		axvline(x=-self.Ly/2. + p5*self.dy, color='r')
		axhline(y=23.-self.Lz/2. + p6*self.dz, color='r')
		axis([-18., 18., 9., 38.])
		colorbar()
		title(r'PDF projection on to yz plane')
		savefig('yz.pdf')

		figure()
		plot(self.zarray, self.psiyz[p5,:], 'b-')
		xlabel('z')
		ylabel('PDF')
		title(r'y-slice of yz plane')
		savefig('yz Cross-section 1.pdf')

		figure()
		plot(self.yarray, self.psiyz[:,p6], 'b-')
		xlabel('y')
		ylabel('PDF')
		title(r'z-slice of yz plane')
		savefig('yz Cross-section 2.pdf')		
		

a = Lorenz(Lx=25., Ly=48., Lz=44.)

a.LoadData()
a.Cumulants2()
a.PDFplots()

show()
