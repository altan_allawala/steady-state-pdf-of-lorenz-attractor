#!/usr/bin/python

import math
from numpy import *
from pylab import *
from scipy import weave
import scipy.interpolate
import scipy.ndimage.interpolation
from scipy.weave import converters
from scipy.integrate import odeint
from matplotlib.colors import LogNorm
from scipy.interpolate import interp1d
from mpl_toolkits.axes_grid1 import make_axes_locatable

# Accumulate the statistics of the stochastically-forced Lorenz attractor by integrating it forward it time using a fourth-order Runge-Kutta time stepper and then save the axial projections of the PDF. Written by Altan Allawala.

class Lorenz (object):
	  
	def __init__(self, x=0.0, y=0.0, z=0.0, gamma = 10., endTime = 100.0, dt = 0.01, N = 100):
		self.dt = dt
		self.gamma = gamma
		self.endTime = endTime
		self.phaseSpace = array([x, y, z])
		print self.phaseSpace
		self.phaseSpacestore = np.zeros((int(self.endTime/self.dt),3))
		self.N = N
		self.Nt = int(self.endTime/self.dt)


	def CreateStochasticForcing(self): # Require that dt<<deltaT<<T
		print "Creating random gaussian array"

		T = 1. #T = 0.1 # Time-scale of the system (this is set by the attractor itself)
		deltaT = sqrt(T*self.dt) # Time-scale over which to vary the stochastic forcing
		Nt = self.Nt # Number of time steps
		coursex = arange(0.,self.endTime+deltaT,deltaT) # Create stochastic points seperated by deltaT
		nt = len(coursex)
		coursef = sqrt(2.0 * self.gamma/deltaT) * np.random.normal(loc=0., scale=1., size = (nt,3))

		print "Interpolating"
		finex = arange(0.,self.endTime,self.dt)
		finefx =  np.interp(finex, coursex, coursef[:,0])
		finefy =  np.interp(finex, coursex, coursef[:,1])
		finefz =  np.interp(finex, coursex, coursef[:,2])

		self.f = concatenate((finefx,finefy,finefz), axis=0)
		self.f = reshape(self.f, (Nt,3), order='F')


	def RHS(self, xarray, f):

		x = xarray[0]
		y = xarray[1]
		z = xarray[2]

		sigma = 3.#10.
		rho = 26.5#28.
		beta = 1.#8./3.

		xdot = sigma * (y - x) + f[0]
		ydot = x * (rho - z) - y + f[1]
		zdot = x * y - beta * z + f[2]
	
		return array([xdot, ydot, zdot])


	def integrate(self): # Fourth Order Runge-Kutta method
		print "Integrating"
		t = 0.
		i = 0
		Nt = self.Nt

		f = self.f

		for i in range(Nt):
			self.k1 = self.dt * self.RHS(self.phaseSpace, f[i,:])
			self.k2 = self.dt * self.RHS(self.phaseSpace+0.5*self.k1, f[i,:])
			self.k3 = self.dt * self.RHS(self.phaseSpace+0.5*self.k2, f[i,:])
			self.k4 = self.dt * self.RHS(self.phaseSpace+self.k3, f[i,:])
			self.phaseSpace = self.phaseSpace + self.k1/6. + self.k2/3. + self.k3/3. + self.k4/6.
			self.phaseSpacestore[i]=self.phaseSpace
			i = i + 1
			if (i%100000==0):
				print "Time = ", i*self.dt


	def Converter(self):

		print "Converting"

		Nt = self.Nt

		self.xmin = -12.5
		self.xmax = 12.5
		self.ymin = -24.
		self.ymax = 24.
		self.zmin = 1.
		self.zmax = 45.

		Lx = self.xmax - self.xmin
		Ly = self.ymax - self.ymin
		Lz = self.zmax - self.zmin

		N = self.N

		self.dx = float(Lx / (N-1))
		self.dy = float(Ly / (N-1))
		self.dz = float(Lz / (N-1))

		dx = self.dx
		dy = self.dy
		dz = self.dz

		self.psi = zeros(shape=(N,N,N))
		psi = self.psi
		
		phaseSpacestore = self.phaseSpacestore

		xmin = float(self.xmin)
		ymin = float(self.ymin)
		zmin = float(self.zmin)
		xmax = float(self.xmax)
		ymax = float(self.ymax)
		zmax = float(self.zmax)

		code = """
			int Px,Py,Pz;
			for (int i = 1; i < Nt; i++) {
				if( (phaseSpacestore(i,0) > xmin) && (phaseSpacestore(i,1) > ymin) && (phaseSpacestore(i,2) > zmin) && (phaseSpacestore(i,0) < xmax) && (phaseSpacestore(i,1) < ymax) && (phaseSpacestore(i,2) < zmax)) {
					Px = round((phaseSpacestore(i,0) - xmin)/dx);
					Py = round((phaseSpacestore(i,1) - ymin)/dy);
					Pz = round((phaseSpacestore(i,2) - zmin)/dz);
					psi(Px,Py,Pz) = psi(Px,Py,Pz) + 1.;
				}
			}
			"""

		weave.inline(code, ['phaseSpacestore', 'psi', 'N', 'Nt', 'dx', 'dy', 'dz', 'xmin', 'ymin', 'zmin', 'xmax', 'ymax', 'zmax'], type_converters=converters.blitz, compiler='gcc')

		self.psi = self.psi / (sum(self.psi)*dx*dy*dz)

		return


	def ProjectOnAxes(self): # Project the PDF onto axes planes
	
		self.psixy = np.sum(self.psi, axis=2)*self.dz
		self.psixz = np.sum(self.psi, axis=1)*self.dy
		self.psiyz = np.sum(self.psi, axis=0)*self.dx

		self.psixy2 = self.psixy.reshape((self.N**2))
		np.savetxt('psixy', self.psixy2)
		self.psixz2 = self.psixz.reshape((self.N**2))
		np.savetxt('psixz', self.psixz2)
		self.psiyz2 = self.psiyz.reshape((self.N**2))
		np.savetxt('psiyz', self.psiyz2)

		print "Saved"


		
p = Lorenz(x=1., y=1., z=25., gamma=0.1, endTime=1000000., dt=.01, N=478)

p.CreateStochasticForcing()
p.integrate()
p.Converter()
p.ProjectOnAxes()
show()
